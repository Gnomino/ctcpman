#!/usr/bin/python3
import socket,ssl,time,re,json,os.path,os
class Bot():
    ENCODING = 'utf-8'
    CTCP_CHAR = "\x01"
    CHAN_HUMILIATION = "#troll"
    censored_words = []
    CENSOR_FILE="censored.json"
    KICK_CENSOR_REASON = ""
    cont = True
    def __init__(self, config):
        self.douchebag = config['douchebag']
        self.censored_words = config['censor']
        self.versions_log = config['version_log']
        self.autovoice = config['autovoice']
        self.commit_file = config['commit_file']
        self.KICK_CENSOR_REASON = config['censor_kick_reason']
        if not os.path.isfile(self.versions_log):
            with open(self.versions_log, 'w+') as f:
                f.write('{}')
        self.socket = socket.create_connection((config['host'], config['port']))
        if config['ssl']:
            self.socket = ssl.wrap_socket(self.socket)
        self.recv()
        self.send("NICK " + config['nick'])
        self.send("USER " + config['nick'] + " 0 * :CTCP Man")
        time.sleep(0.5) # Temporary hack
        self.recv()
        for cmd in config['init_cmds']:
            self.send(cmd)
        for c in config["chans"]:
            self.send("JOIN " + c)
        while self.cont:
            data = self.recv()
            for line in data.split('\r\n'):
                if line:
                    self.parse(line)
                    print(line)
            if os.path.isfile(self.commit_file):
                with open(self.commit_file, 'r') as f:
                    commit_url = f.read()
                os.remove(self.commit_file)
                self.send("QUIT :" + commit_url)
                self.cont = False
    def send(self, msg):
        self.socket.send(msg.encode(self.ENCODING) + "\r\n".encode(self.ENCODING))
        print("> " + msg)
    def sendMsg(self, msg, chan):
        self.send('PRIVMSG ' + chan + ' :' + msg)
    def sendCTCP(self, msg, guy):
        self.sendMsg(self.CTCP_CHAR + msg + self.CTCP_CHAR, guy)
    def recv(self):
        data = self.socket.recv(4096).decode(self.ENCODING, 'ignore')
        return data
    def parse(self, data):
        splitted = data.split(' ', 3)
        if len(splitted) == 4:
            if splitted[1] == 'PRIVMSG':
                sender = splitted[0].split('!')[0][1:]
                channel = splitted[2]
                msg = splitted[3][1:]
                censor = False
                for w in self.censored_words:
                    if w.lower() in msg.lower():
                        censor = True
                if censor:
                    self.send("KICK " + channel + " " + sender + " :" + self.KICK_CENSOR_REASON)
        if len(splitted) == 3:
            if splitted[1] == 'JOIN':
                joiner = splitted[0].split('!')[0][1:]
                chan = splitted[2][1:]
                self.sendCTCP("VERSION", joiner)
                if self.autovoice:
                  self.send("MODE " + chan + " +v " + joiner)
        if len(splitted) >= 3:
            if splitted[1] == 'NOTICE':
                sender = splitted[0].split('!')[0][1:]
                msg = splitted[3][1:]
                if msg[:9] == self.CTCP_CHAR + 'VERSION ':
                    version = msg[9:]
                    version = version[:len(version)-1]
                    if self.versions_log:
                        with open(self.versions_log, 'r') as f:
                            versions = json.loads(f.read())
                        versions[sender] = version
                        with open(self.versions_log, 'w+') as f:
                            f.write(json.dumps(versions, indent=2))
                    if self.douchebag:
                      self.sendMsg("Nan mais " + sender + ", le mec qui utilise " + version, self.CHAN_HUMILIATION)
        if len(splitted) >= 1:
            if splitted[0] == 'PING':
                self.send('PONG ' + " ".join(splitted[1:]))

with open('config.json', 'r') as f:
    config = json.loads(f.read())
Bot(config)
os.system("bash update.sh")
